# obfs4PortScan
This service lets bridge operators test if their bridge's obfs4 port is
publicly reachable.

## Command line arguments arguments
The tool takes a number of command line arguments:

- `-cert-file`: a path to a certificate file to run the HTTPS server
- `-key-file`: a path to a key file to run the HTTPS server
- `-addr`: set the address and port to listen on (defaults to `:4000`)
- `-templatedir`: set the template directory (defaults to `/home/bridgescan/bridge-port-scan/`)

## Scanning method
We try to establish a TCP connection with the given IP address and port using
golang's `net.DialTimeout` function.  If we don't get a response within three
seconds, we deem the port unreachable.  We also deem the port unreachable if we
get a RST segment before the timeout.  In both cases, we display the error
message that we got from `net.DialTimeout`.

We implement a simple rate limiter that limits incoming requests to an average
of one per second with bursts of as many as five requests per second.

## Dependencies
PortScan requires a number of dependencies to build: python3, pip, gettext, venv. These can be installed on debian with `sudo apt install python3 python3-pip gettext python3-venv`

PortScan also requires lektor as a dependency: `pip3 install lektor`

## Building
After installing the dependencies, PortScan can be built by running the `build.sh` script in the project root. Lektor likes to install plugins globally, so activating a virtualenv beforehand is recommended: `python3 -m venv venv && . venv/bin/activate`

The build script will create a `public` directory in the project root. This directory contains the templates and static files used by PortScan, and should be used as the value for the `-templatedir` argument to PortScan.

## Deployment
After running the build script, shut down the obfs4PortScan service on BridgeDB which runs under the
bridgescan user:

    systemctl --user stop obfs4portscan.service

Then, copy the binary onto BridgeDB's host. It belongs into the directory `/home/bridgescan/bin/`. Then copy the `public` directory to your `templatedir` (defaults to `/home/bridgescan/bridge-port-scan`). Once everything is in place, restart the service:

    systemctl --user start obfs4portscan.service
