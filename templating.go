package main

import (
	"html/template"
	"os"
	"path/filepath"
	"strings"
)

func LoadTemplates(template_dir string, template_map map[string]*template.Template, strip_prefix string) error {
	err := filepath.Walk(template_dir, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}

		if !info.IsDir() {
			stripped_path := path
			if strip_prefix != "" && strings.HasPrefix(stripped_path, strip_prefix) {
				stripped_path = stripped_path[len(strip_prefix):]

				// if template_dir doesn't end with a slash, the template keys will be left with a leading slash, so we remove that
				if strings.HasPrefix(stripped_path, "/") {
					stripped_path = stripped_path[1:]
				}
			}

			template_map[stripped_path] = template.Must(template.New(stripped_path).ParseFiles(path))
		}

		return nil
	})

	return err
}
