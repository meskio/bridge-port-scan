#!/usr/bin/env sh

set -ex

git submodule update --init

rm -rf public

cd frontend
rm -rf public
mkdir -p public/templates

cp scan.scss assets/scss/_scan.scss

scan_already_imported=$(cat assets/scss/bootstrap.scss |grep -i '@import "tpo";')

if [ "$scan_already_imported" ]; then
    echo '@import "scan";' >> assets/scss/bootstrap.scss
fi

sass assets/scss:assets/static/css

rm -rf lektoroutput
LEKTOR_ENV=dev lektor b -O lektoroutput

mv lektoroutput/index.html public/templates
mv lektoroutput/failure/index.html public/templates/failure.html
mv lektoroutput/success/index.html public/templates/success.html

mv lektoroutput/static public

mv public ..

cd ..
go build
